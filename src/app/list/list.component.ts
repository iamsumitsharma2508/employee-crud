import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Employee } from "../model/employee";
import { EmployeeService } from "../services/EmployeeService";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
})
export class ListComponent implements OnInit {
  employeeList: Employee[];
  displayedColumns: string[];
  dataSource = new MatTableDataSource<Employee>();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private employeeService: EmployeeService) {
    this.employeeList = new Array<Employee>();
    this.displayedColumns = [
      "id",
      "employee_name",
      "employee_age",
      "employee_salary",
      "createdAt",
      "action",
    ];
  }

  ngOnInit() {
    this.fetchEmployees();
  }

  async fetchEmployees() {
    this.employeeList = await this.employeeService.fetchEmployeeList();
    if (this.employeeList.length > 0) {
      this.employeeList = this.employeeList.filter((x) => x.id).reverse();
    }
    this.dataSource = new MatTableDataSource<Employee>(this.employeeList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event:any) {
    this.dataSource.filter = event.target.value.trim().toLowerCase();
  }
}
