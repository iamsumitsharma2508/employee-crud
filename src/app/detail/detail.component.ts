import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Employee } from "../model/employee";
import { EmployeeService } from "../services/EmployeeService";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"],
})
export class DetailComponent implements OnInit {
  employee: Employee;
  onClickValidation: boolean;
  recordId: number;

  constructor(
    protected route: ActivatedRoute,
    private employeeService: EmployeeService,
    protected router: Router
  ) {
    this.employee = new Employee();
    this.onClickValidation = false;
  }

  ngOnInit() {
    this.recordId = Number(this.route.snapshot.paramMap.get("id"));
    this.getEmployee();
  }

  async getEmployee() {
    this.employee = await this.employeeService.fetchEmployeeById(this.recordId);
  }

  update(form:any) {
    if (!form.valid) {
      this.onClickValidation = true;
      return;
    }
    this.employeeService.updateEmployee(this.employee);
  }
}
