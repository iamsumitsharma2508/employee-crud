import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../model/employee';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(public http: HttpClient, protected router: Router) {}

  fetchEmployeeList() {
    return new Promise<Employee[]>(async (resolve, reject) => {
      try {
        this.http
          .get<any>(
            'http://www.appgrowthcompany.com:5069/api/v1/employee/getAll'
          )
          .subscribe((resp) => {
            if (resp.message == 'Success') {
              resolve(resp.allEmployees);
              return;
            }
            resolve(resp);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  fetchEmployeeById(id: number) {
    return new Promise<Employee>(async (resolve, reject) => {
      try {
        this.http
          .get<any>(
            `http://www.appgrowthcompany.com:5069/api/v1/employee/get/${id}`
          )
          .subscribe((resp) => {
            if (resp.message == 'Success') {
              resolve(resp.data);
              return;
            }
            resolve(resp);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  updateEmployee(data: Employee) {
    return this.http
      .put(
        `http://www.appgrowthcompany.com:5069/api/v1/employee/update/${data.id}`,
        data
      )
      .subscribe((resp: any) => {
        alert(resp.message);
        this.router.navigateByUrl('/');
      });
  }

  addEmployee(data: Employee) {
    return this.http
      .post<any>(
        'http://www.appgrowthcompany.com:5069/api/v1/employee/create',
        data
      )
      .subscribe((resp: any) => {
        alert(resp.message);
        this.router.navigateByUrl('/');
      });
  }
}
