export class Employee {
  id: number;
  employee_age: number;
  employee_name: string;
  employee_salary: string;
  createdAt: string;
}
