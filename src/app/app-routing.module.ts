import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {
    path: "add-employee",
    component: AddComponent
  },
  {
    path: "employee-detail/:id",
    component: DetailComponent
  },
  {
    path: "employee-list",
    component: ListComponent
  },
  {
    path: "",
    component: ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
